package com.example.sumup5.ui.main

import com.example.sumup5.databinding.FragmentInfoBinding


class InfoFragment : BaseFragment<FragmentInfoBinding, MainViewModel>(FragmentInfoBinding::inflate) {
    override fun getViewModelClass() = MainViewModel::class.java
    override var useSharedViewModel = true
    override fun start() {
        viewModel.mapOfItem.observe(viewLifecycleOwner, {
            binding.userName.text = it[1]?.text
        })
    }

}