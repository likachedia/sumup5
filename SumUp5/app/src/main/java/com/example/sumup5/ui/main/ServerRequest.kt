package com.example.sumup5.ui.main

import java.util.*

data class ServerRequest(
        val arr: List<List<Objects>>?
) {
    data class Objects(
        val field_id:Int?,
        val hint:String?,
        val field_type:String?,
        val keyboard:String?,
        val required:Boolean?,
        val is_active:Boolean?,
        val icon:String?,
    ) {}
}
