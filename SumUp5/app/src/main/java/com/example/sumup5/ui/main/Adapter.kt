package com.example.sumup5.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.sumup5.databinding.ModelCardFragmentBinding

class Adapter(private val reqArr: List<List<ServerRequest.Objects>>, val viewModel: MainViewModel): RecyclerView.Adapter<Adapter.ItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adapter.ItemViewHolder {
        return ItemViewHolder(ModelCardFragmentBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
       val model = reqArr[position]
        holder.initRec(model)
    }
     lateinit var secondAdapter: SecondAdapter
     lateinit var rec: RecyclerView
    override fun getItemCount() = reqArr.size
    inner class ItemViewHolder(private val binding: ModelCardFragmentBinding) : RecyclerView.ViewHolder(binding.root) {

        fun initRec(model: List<ServerRequest.Objects>) {
            rec = binding.recyclerSecond
            secondAdapter = SecondAdapter(model, viewModel)
            rec.adapter = secondAdapter
            binding.recyclerSecond.layoutManager = LinearLayoutManager(binding.root.context)

        }
    }

}