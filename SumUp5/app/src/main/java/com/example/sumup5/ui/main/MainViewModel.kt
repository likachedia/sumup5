package com.example.sumup5.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject

fun <T> MutableLiveData<T>.notifyObserver() {
    this.value = this.value
}

class MainViewModel : BaseViewModel() {

    private val _mapOfItem = MutableLiveData<MutableMap<Int, Dataclass>>()
    val mapOfItem: LiveData<MutableMap<Int, Dataclass>>
    get() = _mapOfItem

    init {
        _mapOfItem.value = mutableMapOf()
    }

    fun setData(key: Int, obj: Dataclass) {
        _mapOfItem.value?.set(key, obj)
        _mapOfItem.notifyObserver()
    }

    private lateinit var firstArr: List<List<ServerRequest.Objects>>

    fun parseJson() {
        val jsonArray = JSONArray(jsonData)
        val jsonObject = JSONObject()
        jsonObject.put("arr", jsonArray)
        val request = Gson().fromJson(jsonObject.toString(), ServerRequest::class.java)
        firstArr = request.arr!!
    }

    fun getMap(): MutableMap<Int, Dataclass>? {
        return mapOfItem.value
    }
    fun getArray(): List<List<ServerRequest.Objects>> {
        return firstArr
    }
   private val jsonData = "[ \n" +
            "[ \n" +
            "{ \n" +
            "\"field_id\":1, \n" +
            "\"hint\":\"UserName\", \n" +
            "\"field_type\":\"input\", \n" +
            "\"keyboard\":\"text\", \n" +
            "\"required\":false, \n" +
            "\"is_active\":true, \n" +
            "\"icon\":\"https://jemala.png\" \n" +
            "}, \n" +
            "{ \n" +
            "\"field_id\":2, \n" +
            "\"hint\":\"Email\", \n" +
            "\"field_type\":\"input\", \n" +
            "\"required\":true, \n" +
            "\"keyboard\":\"text\", \n" +
            "\"is_active\":true, \n" +
            "\"icon\":\"https://jemala.png\" \n" +
            "}, \n" +
            "{ \n" +
            "\"field_id\":3, \n" +
            "\"hint\":\"phone\", \n" +
            "\"field_type\":\"input\", \n" +
            "\"required\":true, \n" +
            "\"keyboard\":\"number\", \n" +
            "\"is_active\":true, \n" +
            "\"icon\":\"https://jemala.png\" \n" +
            "} \n" +
            "], \n" +
            "[ \n" +
            "{ \n" +
            "\"field_id\":4,\n" +
            "\"hint\":\"FullName\", \n" +
            "\"field_type\":\"input\", \n" +
            "\"keyboard\":\"text\", \n" +
            "\"required\":true, \n" +
            "\"is_active\":true, \n" +
            "\"icon\":\"https://jemala.png\" }, \n" +
            "{ \n" +
            "\"field_id\":14, \n" +
            "\"hint\":\"Jemali\", \n" +
            "\"field_type\":\"input\", \n" +
            "\"keyboard\":\"text\", \n" +
            "\"required\":false, \n" +
            "\"is_active\":true, \n" +
            "\"icon\":\"https://jemala.png\" }, \n" +
            "{ \n" +
            "\"field_id\":89, \n" +
            "\"hint\":\"Birthday\", \n" +
            "\"field_type\":\"chooser\", \n" +
            "\"required\":false, \n" +
            "\"is_active\":true, \n" +
            "\"icon\":\"https://jemala.png\" }, \n" +
            "{ \n" +
            "\"field_id\":898, \n" +
            "\"hint\":\"Gender\", \n" +
            "\"field_type\":\"chooser\", \n" +
            "\"required\":\"false\", \n" +
            "\"is_active\":true, \n" +
            "\"icon\":\"https://jemala.png\" } \n" +
            "] \n" +
            "]\n"

}