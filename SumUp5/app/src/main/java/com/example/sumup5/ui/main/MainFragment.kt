package com.example.sumup5.ui.main

import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.sumup5.R
import com.example.sumup5.databinding.MainFragmentBinding


class MainFragment : BaseFragment<MainFragmentBinding, MainViewModel>(MainFragmentBinding::inflate) {

    private lateinit var adapter: Adapter
    private var items: List<List<ServerRequest.Objects>> = mutableListOf()

    override fun start() {
        viewModel.parseJson()
        items = viewModel.getArray()
        init()
        addListener()
    }

    private fun init() {
        binding.recycler.layoutManager = LinearLayoutManager(requireContext())
        adapter = Adapter(items, viewModel)
        binding.recycler.adapter = adapter
    }

    private fun addListener() {
        binding.register.setOnClickListener {
            val obj = viewModel.getMap()?.entries
            if (obj != null) {
                var goToNext = true
                for(key in obj) {
                    if(key.value.required == true && key.value.text.isNullOrEmpty()) {
                        Toast.makeText(requireContext(), key.value.hint.plus(resources.getString(R.string.required)), Toast.LENGTH_SHORT).show()
                        goToNext = false
                    }
                }
                if(goToNext) {
                    findNavController().navigate(R.id.action_mainFragment_to_infoFragment)
                }
            }
        }
    }

    override var useSharedViewModel = true
    override fun getViewModelClass() = MainViewModel::class.java
}