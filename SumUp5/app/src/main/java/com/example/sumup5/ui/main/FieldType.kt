package com.example.sumup5.ui.main

enum class FieldType(val type: String) {
    INPUT("input"),
    CHOOSER("chooser"),
    NUMBER("number"),
    TEXT("text")
}
