package com.example.sumup5.ui.main

import androidx.lifecycle.ViewModel

abstract class BaseViewModel: ViewModel()