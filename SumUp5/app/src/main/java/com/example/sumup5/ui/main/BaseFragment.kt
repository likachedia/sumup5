package com.example.sumup5.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding

typealias Inflater<T> = (LayoutInflater, ViewGroup, Boolean) -> T

abstract class BaseFragment<VB: ViewBinding, VM: BaseViewModel>(private val inflate: Inflater<VB>): Fragment() {
    private var _binding:VB? = null
    val binding get() = _binding!!

    open var useSharedViewModel: Boolean = false
    protected lateinit var viewModel: VM
    protected abstract fun getViewModelClass(): Class<VM>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inflate.invoke(inflater, container!!, false)
        viewModel = if (useSharedViewModel) {
            ViewModelProvider(requireActivity())[getViewModelClass()]
        } else {
            ViewModelProvider(this)[getViewModelClass()]
        }

        start()
        return binding.root
    }

    abstract fun start()

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}

