package com.example.sumup5.ui.main

data class Dataclass(
    val field_id:Int?,
    val hint:String?,
    val field_type:String?,
    val keyboard:String?,
    val required:Boolean?,
    val is_active:Boolean?,
    val icon:String?,
    val text:String?,
    val position:Int?
) {
}