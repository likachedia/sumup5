package com.example.sumup5.ui.main


import android.text.InputType
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.sumup5.R
import com.example.sumup5.databinding.ModelChooserBinding
import com.example.sumup5.databinding.ModelItemsFragmentBinding

class SecondAdapter(private val reqArr: List<ServerRequest.Objects>, val viewModel: MainViewModel):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val FIRST_ITEM = 10
        private const val SECOND_ITEM = 11
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):RecyclerView.ViewHolder {
        return if(viewType == FIRST_ITEM)  {
           ItemViewHolder(
                ModelItemsFragmentBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        } else {
            SecondViewHolder(
                ModelChooserBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
       val model = reqArr[position]
        if (holder is ItemViewHolder) {
            holder.onBind(model)

        } else if (holder is SecondViewHolder) {
            holder.onBind(model)

        }

    }

    override fun getItemCount() = reqArr.size

    override fun getItemViewType(position: Int) = if (reqArr[position].field_type == FieldType.CHOOSER.type) SECOND_ITEM else FIRST_ITEM

    inner class ItemViewHolder(private val binding: ModelItemsFragmentBinding) : RecyclerView.ViewHolder(binding.root) {
        fun onBind(model:  ServerRequest.Objects) {
            val id = model.field_id
            binding.etItem.hint= model.hint
            binding.etItem.tag = model.field_id
            binding.etItem.isClickable = model.is_active!!
            Glide.with(itemView.context).load(model.icon).into(binding.Imgicon)
            binding.etItem.doAfterTextChanged {
              val  obj = Dataclass(model.field_id,model.hint, model.field_type, model.keyboard, model.required,model.is_active, model.icon, it.toString(), adapterPosition )
                viewModel.setData(id!!, obj)
            }
            binding.etItem.setText(viewModel.getMap()?.get(id)?.text)
           if(model.required == true) {
               binding.etItem.error = "${model.hint} is required"
           }
            setFieldType(model)
        }

        private fun setFieldType(model:  ServerRequest.Objects) {
            if(model.keyboard == FieldType.NUMBER.type) {
                binding.etItem.inputType = InputType.TYPE_CLASS_NUMBER
            } else if (model.keyboard == FieldType.TEXT.type) {
                binding.etItem.inputType = InputType.TYPE_CLASS_TEXT
            }

        }

    }

    inner class SecondViewHolder(private val binding: ModelChooserBinding): RecyclerView.ViewHolder(binding.root) {

        fun onBind(model:  ServerRequest.Objects) {

            val items = listOf("Option 1", "Option 2", "Option 3", "Option 4")
            val adapter = ArrayAdapter(binding.root.context, R.layout.list_item, items)
            binding.etItem.setAdapter(adapter)
            val id = model.field_id
            binding.etItem.hint= model.hint
            binding.etItem.tag = model.field_id
            binding.etItem.isClickable = model.is_active!!
            Glide.with(itemView.context).load(model.icon).into(binding.Imgicon)

            binding.etItem.doAfterTextChanged {
                val  obj = Dataclass(model.field_id,model.hint, model.field_type, model.keyboard, model.required,model.is_active, model.icon, it.toString(), adapterPosition)
                viewModel.setData(id!!, obj)
            }
            binding.etItem.setText(viewModel.getMap()?.get(id)?.text)
            if(model.required == true) {
                binding.etItem.error = "${model.hint} is required"
            }

      }

    }

}